package com.heima.utils.thread;

import com.heima.model.wemedia.pojos.WmUser;

/**
 * @author jack
 * @data 2023 15:21
 */

public class ApUserThreadLocalUtil {
    private static ThreadLocal<Integer> threadLocal = new ThreadLocal<>();


    //存
    public static void setUser(Integer userId){
        threadLocal.set(userId);
    }

    //取
    public static Integer getUser(){
        return threadLocal.get();
    }

    //删
    public static void clear(){
        threadLocal.remove();
    }
}
