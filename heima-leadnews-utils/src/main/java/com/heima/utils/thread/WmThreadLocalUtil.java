package com.heima.utils.thread;

import com.heima.model.wemedia.pojos.WmUser;

/**
 * @author jack
 * @data 2023 15:21
 */

public class WmThreadLocalUtil {
    private static ThreadLocal<WmUser> threadLocal = new ThreadLocal<>();


    //存
    public static void setUser(WmUser wmUser){
        threadLocal.set(wmUser);
    }

    //取
    public static WmUser getUser(){
        return threadLocal.get();
    }

    //删
    public static void clear(){
        threadLocal.remove();
    }
}
