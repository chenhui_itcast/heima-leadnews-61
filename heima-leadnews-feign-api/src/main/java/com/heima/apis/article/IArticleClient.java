package com.heima.apis.article;

import com.heima.apis.article.fallback.ArticleClientFallback;
import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author jack
 * @data 2023 9:41
 */
@FeignClient(value = "leadnews-article" ,fallback = ArticleClientFallback.class)
public interface IArticleClient {

    //新增或修改app文章
    @PostMapping("/api/v1/article/saveArticle")
    public ResponseResult saveArticle(@RequestBody ArticleDto dto);
}
