package com.heima.apis.article.fallback;

import com.heima.apis.article.IArticleClient;
import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author jack
 * @data 2023 7:50
 */
@Component
@Slf4j
public class ArticleClientFallback implements IArticleClient {
    @Override
    public ResponseResult saveArticle(ArticleDto dto) {
        log.warn("==========保存文章操作降级处理=========");
        log.warn("发送消息到mq，待文章微服务恢复后从mq消费消息，保证业务数据最终一致性!");
        log.warn("发送邮件到管理员");
        return ResponseResult.errorResult(AppHttpCodeEnum.ARTICLE_HYSTRIX_INFO);
    }
}
