package com.heima.apis.schedule;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.schedule.dtos.TaskDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author jack
 * @data 2023 11:39
 */
@FeignClient("leadnews-schedule")
public interface IScheduleClient {
    @PostMapping("/api/v1/schedule/addTask")
    public ResponseResult addTask(@RequestBody TaskDto taskDto);

    @PostMapping("/api/v1/schedule/pullTask/{taskType}/{priority}")
    public ResponseResult pullTask(@PathVariable Integer taskType,
                                   @PathVariable Integer priority);
}
