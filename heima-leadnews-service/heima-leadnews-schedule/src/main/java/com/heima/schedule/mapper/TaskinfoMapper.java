package com.heima.schedule.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.schedule.pojos.Taskinfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author jack
 * @data 2023 9:14
 */
@Mapper
public interface TaskinfoMapper extends BaseMapper<Taskinfo> {
}
