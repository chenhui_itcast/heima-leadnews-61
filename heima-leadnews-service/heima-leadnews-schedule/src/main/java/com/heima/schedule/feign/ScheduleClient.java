package com.heima.schedule.feign;

import com.heima.apis.schedule.IScheduleClient;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.schedule.dtos.TaskDto;
import com.heima.schedule.service.TaskinfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jack
 * @data 2023 12:09
 */
@RestController
public class ScheduleClient implements IScheduleClient {
    @Autowired
    private TaskinfoService taskinfoService;
    /**
     * 添加任务
     *
     * @param taskDto
     * @return
     */
    @PostMapping("/api/v1/schedule/addTask")
    public ResponseResult addTask(@RequestBody TaskDto taskDto) {
        return taskinfoService.addTask(taskDto);
    }

    /**
     * 拉取任务
     *
     * @param taskType
     * @param priority
     * @return
     */
    @PostMapping("/api/v1/schedule/pullTask/{taskType}/{priority}")
    public ResponseResult pullTask(@PathVariable Integer taskType,
                                   @PathVariable Integer priority){
        return taskinfoService.pullTask(taskType,priority);
    }
}
