package com.heima.schedule.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.schedule.pojos.TaskinfoLogs;

/**
 * @author jack
 * @data 2023 9:18
 */
public interface TaskinfoLogsService extends IService<TaskinfoLogs> {
}
