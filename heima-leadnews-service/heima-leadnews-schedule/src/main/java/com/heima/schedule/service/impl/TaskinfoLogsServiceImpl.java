package com.heima.schedule.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.model.schedule.pojos.TaskinfoLogs;
import com.heima.schedule.mapper.TaskinfoLogsMapper;
import com.heima.schedule.service.TaskinfoLogsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author jack
 * @data 2023 9:19
 */
@Service
@Transactional
@Slf4j
public class TaskinfoLogsServiceImpl extends ServiceImpl<TaskinfoLogsMapper, TaskinfoLogs> implements TaskinfoLogsService {
}
