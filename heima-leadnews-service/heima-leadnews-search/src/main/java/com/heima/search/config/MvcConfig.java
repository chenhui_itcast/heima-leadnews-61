package com.heima.search.config;

import com.heima.search.interceptor.ApTokenInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author jack
 * @data 2023 13:06
 */
@Configuration
public class MvcConfig implements WebMvcConfigurer {

    @Autowired
    private ApTokenInterceptor apTokenInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(apTokenInterceptor);
    }
}
