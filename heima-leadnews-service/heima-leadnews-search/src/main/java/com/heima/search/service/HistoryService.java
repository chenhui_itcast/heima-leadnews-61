package com.heima.search.service;

import com.heima.model.common.dtos.ResponseResult;

/**
 * @author jack
 * @data 2023 12:47
 */
public interface HistoryService {
    /**
     * 加载当前用户的历史记录
     *
     * @return
     */
    ResponseResult load();

    /**
     * 新增或更新用户搜索历史记录
     *
     * @param searchWords
     */
    void saveOrUpdate(String searchWords);

}
