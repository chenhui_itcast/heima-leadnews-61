package com.heima.search.interceptor;

import com.heima.model.wemedia.pojos.WmUser;
import com.heima.utils.thread.ApUserThreadLocalUtil;
import com.heima.utils.thread.WmThreadLocalUtil;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author jack
 * @data 2023 15:15
 */
@Component
public class ApTokenInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //解析请求头，将userId存入ThreadLocal
        String userId = request.getHeader("userId");
        if (!StringUtils.isEmpty(userId)){
            ApUserThreadLocalUtil.setUser(Integer.valueOf(userId));
        }

        return true;
    }



    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        //清理ThreadLocal: 一定要及时清理线程数据，否则当访问量过大会导致内存泄露问题
        ApUserThreadLocalUtil.clear();
    }
}
