package com.heima.search.listener;

import com.heima.search.service.ArticleSearchService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * @author jack
 * @data 2023 10:06
 */
@Component
@Slf4j
public class ArticleListener {

    @Autowired
    private ArticleSearchService articleSearchService;

    /**
     * 异步调用和同步调用区分：
     *   1. 如果调用需要有结果返回，那么只能是同步调
     *   2. 如果调用的功能需要立刻执行，该功能必须执行完才能业务接着往下走，
     *   那么只能同步调用。
     *
     *   同步app文章数据
     *
     * @param articleJSON
     */
    @KafkaListener(topics = "ES_ARTICLE_ASYNC_TOPIC")
    public void articleAsync(String articleJSON){
        log.info("接收到同步文章信息的消息，文章数据为：{}",articleJSON);
        if (!StringUtils.isEmpty(articleJSON)){
            articleSearchService.saveArticle(articleJSON);
        }
    }
}
