package com.heima.search.service;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.search.dtos.UserSearchDto;

/**
 * @author jack
 * @data 2023 8:35
 */
public interface ArticleSearchService {

    /**
     * app文章搜索
     *
     * @param searchDto
     * @return
     */
    ResponseResult search(UserSearchDto searchDto);

    /**
     * app文章数据同步
     *
     * @param articleJSON
     */
    void saveArticle(String articleJSON);
}
