package com.heima.search.controller;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.search.service.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jack
 * @data 2023 12:44
 */
@RestController
@RequestMapping("/api/v1/history")
public class HistoryController {

    @Autowired
    private HistoryService historyService;

    /**
     * app搜索记录加载
     *
     * @return
     */
    @PostMapping("load")
    public ResponseResult load(){
        return historyService.load();
    }
}
