package com.heima.search.config;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author jack
 * @data 2023 11:41
 */
@Configuration
public class RestHighLevelClientConfig {
    @Value("${elasticsearch.host}")
    private String ip;
    @Value("${elasticsearch.port}")
    private String port;


    @Bean
    public RestHighLevelClient restHighLevelClient(){
        RestHighLevelClient client =  new RestHighLevelClient(RestClient.builder(
                HttpHost.create("http://"+ip+":"+port)
        ));
        return client;
    }
}
