package com.heima.search.controller;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.search.dtos.UserSearchDto;
import com.heima.search.service.ArticleSearchService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jack
 * @data 2023 8:00
 */
@RestController
@RequestMapping("/api/v1/article/search")
@Slf4j
public class ArticleSearchController {
    @Autowired
    private ArticleSearchService articleSearchService;

    /**
     * app 文章搜索
     *
     * @param searchDto
     * @return
     */
    @PostMapping("/search")
    public ResponseResult search(@RequestBody UserSearchDto searchDto){
        log.info("app文章搜索接口被请求了，请求参数：{}",searchDto);
        return articleSearchService.search(searchDto);
    }
}
