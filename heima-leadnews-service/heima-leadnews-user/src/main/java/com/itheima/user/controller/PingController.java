package com.itheima.user.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jack
 * @data 2023 9:16
 */
@RestController
@RequestMapping("/user")
public class PingController {

    @GetMapping("/ping")
    public String ping(){
        return "jenkins so cool~";
    }
}
