package com.heima.wemedia.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.wemedia.pojos.WmMaterial;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author jack
 * @data 2023 11:46
 */
@Mapper
public interface WmMaterialMapper extends BaseMapper<WmMaterial> {
}
