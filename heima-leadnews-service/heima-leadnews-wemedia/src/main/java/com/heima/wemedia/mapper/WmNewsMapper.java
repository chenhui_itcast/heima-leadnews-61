package com.heima.wemedia.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.wemedia.pojos.WmNews;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author jack
 * @data 2023 11:05
 */
@Mapper
public interface WmNewsMapper extends BaseMapper<WmNews> {
}
