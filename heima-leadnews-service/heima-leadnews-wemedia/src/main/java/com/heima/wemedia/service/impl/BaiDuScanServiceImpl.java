package com.heima.wemedia.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.heima.common.redis.CacheService;
import com.heima.file.service.FileStorageService;
import com.heima.utils.baidu.Base64Util;
import com.heima.utils.baidu.HttpUtil;
import com.heima.wemedia.service.BaiDuScanService;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.net.URLEncoder;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author jack
 * @data 2023 12:15
 */
@Service
@Slf4j
public class BaiDuScanServiceImpl implements BaiDuScanService {

    @Autowired
    private FileStorageService fileStorageService;
    @Autowired
    private CacheService cacheService;

    static final OkHttpClient HTTP_CLIENT = new OkHttpClient().newBuilder().build();
    /**
     * 百度云内容安全审核 - 文本审核
     *
     * @param text
     * @return
     */
    @Override
    public Map greenScanText(String text) {
        // 请求url
        String url = "https://aip.baidubce.com/rest/2.0/solution/v1/text_censor/v2/user_defined";
        try {
            String param = "text=" + URLEncoder.encode(text, "utf-8");

            // 注意这里仅为了简化编码每一次请求都去获取access_token，线上环境access_token有过期时间， 客户端可自行缓存，过期后重新获取。
            String accessToken = cacheService.get("accessToken");
            if (StringUtils.isEmpty(accessToken)){
                accessToken = getAccessToken();
                cacheService.setEx("accessToken",accessToken,10, TimeUnit.DAYS);
            }

            String result = HttpUtil.post(url, accessToken, param);
            log.info(result);


            return JSON.parseObject(result,Map.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 百度云内容安全审核 - 图片审核
     *
     * @param imgUrl
     * @return
     */
    @Override
    public Map greenScanImg(String imgUrl) {
        // 请求url
        String url = "https://aip.baidubce.com/rest/2.0/solution/v1/img_censor/v2/user_defined";
        try {
            byte[] imgData = fileStorageService.downLoadFile(imgUrl);
            String imgStr = Base64Util.encode(imgData);
            String imgParam = URLEncoder.encode(imgStr, "UTF-8");

            String param = "image=" + imgParam;

            // 注意这里仅为了简化编码每一次请求都去获取access_token，线上环境access_token有过期时间， 客户端可自行缓存，过期后重新获取。
            String accessToken = cacheService.get("accessToken");
            if (StringUtils.isEmpty(accessToken)){
                accessToken = getAccessToken();
                cacheService.setEx("accessToken",accessToken,10, TimeUnit.DAYS);
            }

            String result = HttpUtil.post(url, accessToken, param);
            log.info(result);
            return JSON.parseObject(result,Map.class);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getAccessToken(){
        try {
            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, "");
            Request request = new Request.Builder()
                    .url("https://aip.baidubce.com/oauth/2.0/token?client_id=FYxpANDUMANQF4TxBBBOsHHW&client_secret=Gc3YZnjqYmCpoCdcBtcTG7cHgfQka0HX&grant_type=client_credentials")
                    .method("POST", body)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Accept", "application/json")
                    .build();
            Response response = HTTP_CLIENT.newCall(request).execute();
            String result = response.body().string();
            return JSONObject.parseObject(result).getString("access_token");
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
