package com.heima.wemedia.controller.v1;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jack
 * @data 2023 15:52
 */
@RestController
@RequestMapping("/jvm")
public class JvmController {

    @GetMapping("/test")
    public ResponseResult test(){
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

}
