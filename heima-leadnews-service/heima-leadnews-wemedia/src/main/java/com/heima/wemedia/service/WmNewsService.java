package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.WmNewsDto;
import com.heima.model.wemedia.dtos.WmNewsPageReqDto;
import com.heima.model.wemedia.pojos.WmNews;

/**
 * @author jack
 * @data 2023 11:05
 */
public interface WmNewsService extends IService<WmNews> {
    /**
     * 查询内容列表 - 多条件分页查询
     *
     * @param wmNewsPageReqDto
     * @return
     */
    ResponseResult findByPage(WmNewsPageReqDto wmNewsPageReqDto);

    /**
     * 发布文章
     *
     * @param dto
     * @return
     */
    ResponseResult submit(WmNewsDto dto);

    /**
     * 文章上下架
     *
     * @param dto
     * @return
     */
    ResponseResult downOrUp(WmNewsDto dto);
}
