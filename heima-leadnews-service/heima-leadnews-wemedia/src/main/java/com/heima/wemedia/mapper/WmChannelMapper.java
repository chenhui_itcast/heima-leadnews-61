package com.heima.wemedia.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.wemedia.pojos.WmChannel;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author jack
 * @data 2023 9:18
 */
@Mapper
public interface WmChannelMapper extends BaseMapper<WmChannel> {
}
