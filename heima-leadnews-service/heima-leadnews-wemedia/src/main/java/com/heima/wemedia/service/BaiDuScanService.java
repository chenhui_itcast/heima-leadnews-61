package com.heima.wemedia.service;

import java.util.Map;

/**
 * @author jack
 * @data 2023 12:13
 */
public interface BaiDuScanService {

    /**
     * 百度云内容安全审核 - 文本审核
     *
     * @param text
     * @return
     */
    Map greenScanText(String text);

    /**
     * 百度云内容安全审核 - 图片审核
     *
     * @param imgUrl
     * @return
     */
    Map greenScanImg(String imgUrl);
}
