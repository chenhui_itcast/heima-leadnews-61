package com.heima.wemedia.service;

/**
 * @author jack
 * @data 2023 13:37
 */
public interface WmAutoScanService {

    void scanNews(Integer newsId);
}
