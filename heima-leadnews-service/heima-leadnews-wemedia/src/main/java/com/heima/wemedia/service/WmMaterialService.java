package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.WmMaterialDto;
import com.heima.model.wemedia.pojos.WmMaterial;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author jack
 * @data 2023 11:46
 */
public interface WmMaterialService extends IService<WmMaterial> {
    /**
     * 上传素材
     *
     * @param multipartFile
     * @return
     */
    ResponseResult upload(MultipartFile multipartFile);

    /**
     * 分页条件查询
     * @param dto
     * @return
     */
    ResponseResult queryByPage(WmMaterialDto dto);
}
