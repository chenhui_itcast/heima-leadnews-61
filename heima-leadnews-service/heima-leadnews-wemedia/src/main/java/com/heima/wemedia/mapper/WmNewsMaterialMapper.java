package com.heima.wemedia.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.wemedia.pojos.WmNewsMaterial;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author jack
 * @data 2023 15:09
 */
@Mapper
public interface WmNewsMaterialMapper extends BaseMapper<WmNewsMaterial> {
    //批量保存文章和素材关系
    void saveNewsMaterials(@Param("materialIds") List<Integer> materialIds,
                           @Param("newsId") Integer newsId,
                           @Param("type") Short type);
}
