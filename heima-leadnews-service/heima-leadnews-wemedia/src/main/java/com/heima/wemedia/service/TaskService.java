package com.heima.wemedia.service;

import com.heima.model.wemedia.pojos.WmNews;

/**
 * @author jack
 * @data 2023 11:34
 */
public interface TaskService {
    /**
     * 远程调用延迟微服务，进行审核文章任务添加
     *
     * @param wmNews
     */
    void addTask(WmNews wmNews);

    void pullTask();
}
