package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.pojos.WmChannel;

/**
 * @author jack
 * @data 2023 9:20
 */
public interface WmChannelService extends IService<WmChannel> {
    /**
     * 查询所有频道数据
     * tips: 使用redis优化性能
     *
     * @return
     */
    ResponseResult channels();
}
