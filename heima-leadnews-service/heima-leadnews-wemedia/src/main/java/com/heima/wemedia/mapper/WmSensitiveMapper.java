package com.heima.wemedia.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.wemedia.pojos.WmSensitive;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author jack
 * @data 2023 14:29
 */
@Mapper
public interface WmSensitiveMapper extends BaseMapper<WmSensitive> {
}
