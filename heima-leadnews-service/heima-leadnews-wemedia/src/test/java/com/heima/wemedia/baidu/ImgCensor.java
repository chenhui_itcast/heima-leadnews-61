package com.heima.wemedia.baidu;

import com.heima.file.service.FileStorageService;
import com.heima.utils.baidu.Base64Util;
import com.heima.utils.baidu.FileUtil;
import com.heima.utils.baidu.HttpUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import java.net.URLEncoder;

/**
* 图像审核接口
*/
@SpringBootTest
@RunWith(SpringRunner.class)
public class ImgCensor {
    @Autowired
    private FileStorageService fileStorageService;

   @Test
    public void ImgCensor() {
        // 请求url
        String url = "https://aip.baidubce.com/rest/2.0/solution/v1/img_censor/v2/user_defined";
        try {
            // 本地文件路径
//            String filePath = "C:\\Users\\63471\\Pictures\\14528359d764406b85b00632a48bed3d.png";
//            byte[] imgData = FileUtil.readFileByBytes(filePath);

            String filePath = "http://192.168.200.130:9000/leadnews/2021/04/26/a73f5b60c0d84c32bfe175055aaaac40.jpg";
            byte[] imgData = fileStorageService.downLoadFile(filePath);
            String imgStr = Base64Util.encode(imgData);
            String imgParam = URLEncoder.encode(imgStr, "UTF-8");

            String param = "image=" + imgParam;

            // 注意这里仅为了简化编码每一次请求都去获取access_token，线上环境access_token有过期时间， 客户端可自行缓存，过期后重新获取。
            String accessToken = "24.538b86b7085f919f8939d64a0c0fa944.2592000.1702093719.282335-42655602";

            String result = HttpUtil.post(url, accessToken, param);
            System.out.println(result);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}