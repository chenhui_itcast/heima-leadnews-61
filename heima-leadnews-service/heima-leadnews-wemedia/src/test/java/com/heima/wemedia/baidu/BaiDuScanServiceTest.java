package com.heima.wemedia.baidu;

import com.heima.wemedia.service.BaiDuScanService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author jack
 * @data 2023 12:26
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class BaiDuScanServiceTest {
    @Autowired
    private BaiDuScanService baiDuScanService;

    @Test
    public void testScanText(){
        System.out.println(baiDuScanService.greenScanText("oh, my sheet! it's so cool~"));
    }
}
