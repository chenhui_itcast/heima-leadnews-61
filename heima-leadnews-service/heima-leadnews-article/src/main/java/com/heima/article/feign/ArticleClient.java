package com.heima.article.feign;

import com.heima.apis.article.IArticleClient;
import com.heima.article.service.ApArticleService;
import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jack
 * @data 2023 10:30
 */
@RestController
public class ArticleClient implements IArticleClient {
    @Autowired
    private ApArticleService apArticleService;

    /**
     * 同步自媒体文章到app端
     *
     * @param dto
     * @return
     */
    @PostMapping("/api/v1/article/saveArticle")
    public ResponseResult saveArticle(@RequestBody ArticleDto dto) {
       /* try {
            Thread.sleep(1000000000000000000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

        return apArticleService.saveArticle(dto);
    }
}
