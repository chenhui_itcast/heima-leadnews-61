package com.heima.article.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.article.pojos.ApArticleContent;

/**
 * @author 陈辉
 * @data 2023 11:26
 */
public interface ApArticleContentService extends IService<ApArticleContent> {
}
