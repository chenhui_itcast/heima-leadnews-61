package com.heima.article.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.article.mapper.ApArticleConfigMapper;
import com.heima.article.service.ApArticleConfigService;
import com.heima.model.article.pojos.ApArticleConfig;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author 陈辉
 * @data 2023 11:28
 */
@Service
public class ApArticleConfigServiceImpl extends ServiceImpl<ApArticleConfigMapper, ApArticleConfig> implements ApArticleConfigService {
    /**
     * 文章上下架
     *
     * @param map
     */
    @Override
    public void downOrUp(Map map) {
        String aritlceId = map.get("aritlceId").toString();
        String enable = map.get("enable").toString();

        boolean isDown = false;     //假设要上架
        if (enable.equals("0")){
            isDown = true;
        }

        ApArticleConfig config = getOne(Wrappers.<ApArticleConfig>lambdaQuery()
                .eq(ApArticleConfig::getArticleId, aritlceId));
        config.setIsDown(isDown);
        updateById(config);
    }
}
