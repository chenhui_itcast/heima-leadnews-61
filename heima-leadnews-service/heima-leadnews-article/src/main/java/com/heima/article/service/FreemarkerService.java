package com.heima.article.service;

import com.heima.model.article.dtos.ArticleDto;

/**
 * @author jack
 * @data 2023 16:25
 */
public interface FreemarkerService {
    /**
     * 构建文章详情页
     *
     * @param content 文章内容
     * @param articleId 文章id
     */
    void buildDetailsPage(String content, Long articleId);
}
