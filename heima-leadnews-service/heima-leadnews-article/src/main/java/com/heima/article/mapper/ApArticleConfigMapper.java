package com.heima.article.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.article.pojos.ApArticleConfig;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 陈辉
 * @data 2023 11:24
 */
@Mapper
public interface ApArticleConfigMapper extends BaseMapper<ApArticleConfig> {
}
