package com.heima.article.listener;

import com.alibaba.fastjson.JSON;
import com.heima.article.service.ApArticleConfigService;
import com.heima.article.service.ApArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author jack
 * @data 2023 13:52
 */
@Component
public class ArticleListener {

    @Autowired
    private ApArticleConfigService apArticleConfigService;

    /**
     * article端文章上下架
     *
     * @param msg
     */
    @KafkaListener(topics = "down_or_up_article_topic")
    public void downOrUp(String msg){
        Map map = JSON.parseObject(msg, Map.class);
        apArticleConfigService.downOrUp(map);
    }
}
