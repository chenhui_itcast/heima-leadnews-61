package com.heima.article.job;

import com.heima.article.service.HotArticleService;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author jack
 * @data 2023 14:17
 */
@Component
@Slf4j
public class HotArticleJob {

    @Autowired
    private HotArticleService hotArticleService;

    @XxlJob("hotArticleHandler")
    public void hotArticleExecute(){
        log.info("热点文章开始计算.....");

        hotArticleService.hotArticle();

        log.info("热点文章计算结束.....");
    }
}
