package com.heima.article;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.article.mapper.ApArticleContentMapper;
import com.heima.article.mapper.ApArticleMapper;
import com.heima.file.service.FileStorageService;
import com.heima.model.article.pojos.ApArticle;
import com.heima.model.article.pojos.ApArticleContent;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 陈辉
 * @data 2023 15:42
 */
@SpringBootTest
@RunWith(SpringRunner.class)   //分布式环境下单元测试需要添加此注解，否则：ioc环境可能会有缺失
public class ArticleApplicationTest {

    @Autowired
    private ApArticleContentMapper apArticleContentMapper;
    @Autowired
    private ApArticleMapper apArticleMapper;
    @Autowired
    private Configuration configuration;
    @Autowired
    private FileStorageService fileStorageService;

    /**
     * 需求： 给id是1302862387124125698 的文章
     *        生成文章详情页面并上传到MinIO
     */
    @Test
    public void articleDetail() throws Exception {
        // 第一步： 生成文章详情页
        //1. 拿到文章内容
        ApArticleContent content = apArticleContentMapper.selectOne(
                Wrappers.<ApArticleContent>lambdaQuery().eq(ApArticleContent::getArticleId,"1302862387124125698"));

        Map<String, Object> map = new HashMap<>();
        map.put("content", JSON.parseArray(content.getContent()));

        //2. 拿到文章模板
        Template template = configuration.getTemplate("article.ftl");

        //3. 进行文章详情页合成
        StringWriter sw = new StringWriter();
        template.process(map,sw);

        //第二步： 将详情页上传到minIO
        InputStream is = new ByteArrayInputStream(sw.toString().getBytes());
        String path = fileStorageService.uploadHtmlFile("", "1302862387124125698.html", is);

        //第三步： 将minIO中的详情页地址回填到ap_article表的static_url上
        ApArticle article = new ApArticle();
        article.setId(1302862387124125698L);
        article.setStaticUrl(path);
        apArticleMapper.updateById(article);        // update ap_article set static_url = ? where id = ?;
    }
}
