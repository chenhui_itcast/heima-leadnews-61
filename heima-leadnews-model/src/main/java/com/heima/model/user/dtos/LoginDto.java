package com.heima.model.user.dtos;

import lombok.Data;

/**
 * @author 陈辉
 * @data 2023 16:22
 */
@Data
public class LoginDto {
    private String phone;   //手机号
    private String password;  //密码
}
