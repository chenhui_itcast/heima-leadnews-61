package com.heima.model.article.vos;

import com.heima.model.article.pojos.ApArticle;
import lombok.Data;

/**
 * @author jack
 * @data 2023 15:03
 */
@Data
public class ApArticleVo extends ApArticle {
    private int score;      //分数
}
