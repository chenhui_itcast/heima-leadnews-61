package com.heima.controller;

import com.heima.pojo.Clazz;
import com.heima.pojo.Student;
import com.heima.service.JVMService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

/**
 * @author 陈辉
 * @data 2023 10:36
 */
@RestController
public class JVMController {

    @Autowired
    private JVMService jvmService;

    @GetMapping("/jvm")
    public String testJVM(String msg) {
        if ("abc".equals(msg)) {
            //只要对象被栈内存中的变量引用，那么这个对象称之为：GC Root对象
//            while ("abc".equals(msg)){
                Student stu = new Student();
                stu.setClz(new Clazz(new String("61期")));
//            }

            /*for (int i = 0; i < 1_000_000; i++) {
                list.add(new Student());  //被GC root对象关联到的对象，称之为：强对象     软  弱  虚
            }*/


       /*
       Thread t1 = new Thread(() -> {
            synchronized ("abc") {
                try {
                    Thread.sleep(1000);

                    synchronized ("cba") {
                        System.out.println("线程1获得了所有锁");
                    }
                    ;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });


        Thread t2 = new Thread(() -> {
            synchronized ("cba") {
                try {
                    Thread.sleep(1000);

                    synchronized ("abc") {
                        System.out.println("线程2获得了所有锁");
                    }
                    ;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        t1.setName("t1");
        t2.setName("t2");

        t1.start();
        t2.start();
        */
//        jvmService.testSleep();

            //int a = 1 / 0 ;
            return "error";
        }else {
            return "ok";
        }


    }
}
