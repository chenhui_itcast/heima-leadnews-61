package com.heima.controller;


import java.util.Random;
import java.util.concurrent.CountDownLatch;

/*
    马云，马化腾，张一鸣，李强 四个人一起开会；每个人入会时间随机的，要求：必须所有人到齐，会议才能开始。
 */
public class CountDownLatchDemo {
    public static void main(String[] args) {
        CountDownLatch latch = new CountDownLatch(4);

        //马云
        new Thread(()->{
            try {
                Thread.sleep(new Random().nextInt(5) * 1000);
                System.out.println("马云到了.....");
                latch.countDown();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        //马华腾
        new Thread(()->{
            try {
                Thread.sleep(new Random().nextInt(5) * 1000);
                System.out.println("马化腾到了.....");
                latch.countDown();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        //张一鸣
        new Thread(()->{
            try {
                Thread.sleep(new Random().nextInt(5) * 1000);
                System.out.println("张一鸣到了.....");
                latch.countDown();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        //李强
        new Thread(()->{
            try {
                Thread.sleep(new Random().nextInt(5) * 1000);
                System.out.println("李强到了.....");
                latch.countDown();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();


        try {
            latch.await();      //让当前线程对门栓进行等待
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("人到齐了，会议开始！");
    }
}
