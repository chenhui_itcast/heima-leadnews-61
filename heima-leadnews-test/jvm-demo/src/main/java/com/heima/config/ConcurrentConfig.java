package com.heima.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.Semaphore;

/**
 * @author 陈辉
 * @data 2023 15:37
 */
@Configuration
public class ConcurrentConfig {

    @Bean
    public Semaphore semaphore(){
        return new Semaphore(2);
    }
}
