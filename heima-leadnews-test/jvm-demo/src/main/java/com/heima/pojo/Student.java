package com.heima.pojo;

/**
 * @author 陈辉
 * @data 2023 16:35
 */

public class Student {
    private String name;
    private int age;
    private Clazz clz;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void study(){
        System.out.println("学习");
    }

    public Clazz getClz() {
        return clz;
    }

    public void setClz(Clazz clz) {
        this.clz = clz;
    }
}
