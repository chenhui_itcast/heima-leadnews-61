package com.heima.service;

import org.springframework.stereotype.Service;

/**
 * @author 陈辉
 * @data 2023 10:49
 */
@Service
public class JVMService {

    public void testSleep(){
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
