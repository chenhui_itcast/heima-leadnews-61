package com.heima;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 陈辉
 * @data 2023 10:34
 */
@SpringBootApplication
public class JVMApplication {
    public static void main(String[] args) {
        SpringApplication.run(JVMApplication.class,args);
    }
}
