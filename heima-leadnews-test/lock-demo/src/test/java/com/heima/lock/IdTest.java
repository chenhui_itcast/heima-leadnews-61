package com.heima.lock;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author jack
 * @data 2023 15:24
 */
@SpringBootTest
public class IdTest {


    @Test
    public void testId(){

        for (int i = 1; i <= 10; i++) {
            long id = IdWorker.getId();
            System.out.println("第"+i+"个id = " + id);
        }

    }
}
