package com.heima.lock.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.lock.pojos.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author jack
 * @data 2023 14:43
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
}
