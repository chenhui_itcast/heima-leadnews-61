package com.heima.lock.pojos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author jack
 * @data 2023 11:26
 */
@Data
@TableName("mylock")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MyLock {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String threadName;
    private String lockName;
}
