package com.heima.lock.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.lock.pojos.MtOrder;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author jack
 * @data 2023 10:26
 */
@Mapper
public interface MtOrderMapper extends BaseMapper<MtOrder> {
}
