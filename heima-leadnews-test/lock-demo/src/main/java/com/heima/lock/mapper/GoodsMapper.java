package com.heima.lock.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.lock.pojos.Goods;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author jack
 * @data 2023 14:42
 */
@Mapper
public interface GoodsMapper extends BaseMapper<Goods> {
}
