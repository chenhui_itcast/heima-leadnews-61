package com.heima.lock.pojos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @author jack
 * @data 2023 10:24
 */
@Data
@TableName("mt_order")
public class MtOrder {
    @TableId(type = IdType.AUTO)
    private Integer id;

    private String phone;
    private String department;
    private String doctor;
    private Date time;
    private String name;
    private String num;
}
