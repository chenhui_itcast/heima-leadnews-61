package com.itheima.freemark;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 陈辉
 * @data 2023 10:46
 */
@SpringBootApplication
public class FreemarkApplication {
    public static void main(String[] args) {
        SpringApplication.run(FreemarkApplication.class,args);
    }
}
