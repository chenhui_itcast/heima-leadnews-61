package com.heima.sjms.工厂.手写IOC;

/**
 * @author jack
 * @data 2023 10:35
 */
public interface StudentService {
    void save();
}

@Service
class StudentServiceImpl1 implements StudentService{
    @Override
    public void save() {
        System.out.println("StudentServiceImpl1.....save......");
    }
}

@Service
class StudentServiceImpl2 implements StudentService{
    @Override
    public void save() {
        System.out.println("StudentServiceImpl2.....save......");
    }
}
