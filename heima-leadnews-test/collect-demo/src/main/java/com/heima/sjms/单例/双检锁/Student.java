package com.heima.sjms.单例.双检锁;

/**
 * 单例：多次操作，使用的是同一个对象
 *
 * static: 被static修饰的成员属于类，被该类的所有对象共享使用
 *  final: 最终态
 */

public class Student {
}


class Demo{
    private static Student stu;

    public static void main(String[] args) {
        //t1
        new Thread(()->{
            Student t1 = getStu();
            System.out.println("t1 = " + t1);
        }).start();

        //t2
        new Thread(()->{
            Student t2 = getStu();
            System.out.println("t2 = " + t2);
        }).start();
    }

    //懒汉式：不用不创建，用的时候再创建
    //双检锁
    public static Student getStu(){
        synchronized ("abc") {
            if (stu == null){
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                stu = new Student();
            }
        }
        return stu;
    }

}
