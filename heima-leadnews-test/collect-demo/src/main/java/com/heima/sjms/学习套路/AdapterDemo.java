package com.heima.sjms.学习套路;

// 目标接口
interface Target {
    int request();
}

// 需要被适配的类
class Adaptee {
    int specificRequest() {
        System.out.println("使用家庭电压220V");
        return 220;
    }
}

// 适配器类
class Adapter implements Target {
    private Adaptee adaptee;

    public Adapter(Adaptee adaptee) {
        this.adaptee = adaptee;
    }

    @Override
    public int request() {
        System.out.println("适配器转换：");
        int value = adaptee.specificRequest();
        value /= 44;
        System.out.println("经过适配器转换后，使用电压"+value+"V进行手机充电");
        return value;
    }
}

// 测试类
public class AdapterDemo {
    public static void main(String[] args) {
        Adaptee adaptee = new Adaptee();
        Target target = new Adapter(adaptee);
        target.request();
    }
}