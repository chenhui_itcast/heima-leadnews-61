package com.heima.sjms.工厂.简单工厂;

/**
 * @author jack
 * @data 2023 10:15
 */

public class Student {
}


/**
 *  学生工厂
 *      职责：专门生产学生对象
 */
class StudentFactory{

    public static Student getStu(){
        return new Student();
    }

}
