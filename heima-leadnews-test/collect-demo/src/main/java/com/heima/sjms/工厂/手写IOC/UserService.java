package com.heima.sjms.工厂.手写IOC;

/**
 * @author jack
 * @data 2023 10:34
 */
public interface UserService {
    void login();
}

@Service
class UserServiceImpl1 implements UserService{
    @Override
    public void login() {
        System.out.println("UserServiceImpl1.....login.....");
    }
}

@Service
class UserServiceImpl2 implements UserService{
    @Override
    public void login() {
        System.out.println("UserServiceImpl2.....login.....");
    }
}
