package com.heima.sjms.单例.饿汉式;

/**
 * 单例：多次操作，使用的是同一个对象
 *
 * static: 被static修饰的成员属于类，被该类的所有对象共享使用
 *  final: 最终态
 */

public class Student {
}


class Demo{
    private static Student stu;

    //静态代码块：随着类的加载而执行，只执行一次
    static {
        if (stu == null){
            stu = new Student();
        }
    }

    public static void main(String[] args) {
        Student stu1 = getStu();
        Student stu2 = getStu();

        System.out.println(stu1 == stu2);
    }

    //饿汉式：不用也提前创建，用的时候直接用
    public static Student getStu(){
        return stu;
    }

}
