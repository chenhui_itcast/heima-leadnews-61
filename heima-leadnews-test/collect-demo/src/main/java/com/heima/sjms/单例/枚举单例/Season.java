package com.heima.sjms.单例.枚举单例;

import com.heima.sjms.单例.单例被破坏.Student;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * @author jack
 * @data 2023 9:36
 */
public enum Season {
    SPRING;          //枚举项：就是当前枚举类的对象!!!
    private Season(){}
}


class Demo{
    public static void main(String[] args) throws Exception {
        Season s1 = Season.SPRING;
        Season s2 = Season.SPRING;
        System.out.println(s1 == s2);


        //基于反射破坏单例
       /* Class clazz = s1.getClass();
        Constructor constructor = clazz.getDeclaredConstructor();
        constructor.setAccessible(true);
        Season s3 = (Season) constructor.newInstance();
        System.out.println("s3 = " + s3);*/


        //基于序列化方式来破坏
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("D://a.txt"));
        oos.writeObject(s1);
        oos.close();

        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("D://a.txt"));
        Season s3 = (Season) ois.readObject();
        ois.close();
        System.out.println("s3 = " + s3);

    }
}
