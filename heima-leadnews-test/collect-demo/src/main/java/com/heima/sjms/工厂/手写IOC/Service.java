package com.heima.sjms.工厂.手写IOC;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author jack
 * @data 2023 10:38
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Service {
    String value() default "";
}
