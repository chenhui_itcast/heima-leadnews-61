package com.heima.sjms.原型;

import lombok.Data;

/**
 * @author jack
 * @data 2023 14:03
 */
@Data
public class Student implements Cloneable{
    private String name;
    private int age;

    private Cls cls;

    @Override
    protected Student clone() throws CloneNotSupportedException {
        Student clone = (Student) super.clone();
        Cls cls = clone.cls.clone();
        clone.setCls(cls);
        return clone;
    }
}

@Data
class Cls implements Cloneable{
    private String cname;

    @Override
    protected Cls clone() throws CloneNotSupportedException {
        return (Cls) super.clone();
    }
}
