package com.heima.sjms.单例.懒汉式;

/**
 * 单例：多次操作，使用的是同一个对象
 *
 * static: 被static修饰的成员属于类，被该类的所有对象共享使用
 *  final: 最终态
 */

public class Student {
}


class Demo{
    private static Student stu;

    public static void main(String[] args) {
        Student stu1 = getStu();
        Student stu2 = getStu();

        System.out.println(stu1 == stu2);
    }

    //懒汉式：不用不创建，用的时候再创建
    public static Student getStu(){
        if (stu == null){
            stu = new Student();
        }
        return stu;
    }

}
