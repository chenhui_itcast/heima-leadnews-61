package com.heima.sjms.原型;

/**
 * 原型模式：以一个对象为原型，快速构建出一个和他属性相同的对象。  应用：Spring --> scope: prototype
 *  浅拷贝： 仅拷贝当前对象本身，当前对象中所引用的其他对象不做拷贝,直接地址复用。  -- 低开销
 *  深拷贝： 不仅拷贝当前对象本身，当前对象中所引用的其他对象也做拷贝。  -- 高开销
 */

public class Demo {
    public static void main(String[] args) throws Exception {
        Student stu1 = new Student();
        stu1.setName("zs");
        stu1.setAge(18);

        Cls cls = new Cls();
        cls.setCname("JavaEE61期");
        stu1.setCls(cls);

        System.out.println("stu1 = " + stu1);

        //需求：基于stu1为原型对象 快速创建出stu2
        Student stu2 = stu1.clone();
        System.out.println("stu2 = " + stu2);

    }
}
