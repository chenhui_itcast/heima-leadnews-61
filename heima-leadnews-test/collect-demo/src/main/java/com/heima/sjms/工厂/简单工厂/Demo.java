package com.heima.sjms.工厂.简单工厂;

/**
 * @author jack
 * @data 2023 10:15
 */

public class Demo {

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            Student stu = StudentFactory.getStu();
            System.out.println("stu = " + stu);
        }
    }

}
