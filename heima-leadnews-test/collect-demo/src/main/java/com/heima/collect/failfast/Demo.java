package com.heima.collect.failfast;

import java.lang.invoke.ConstantCallSite;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * 集合的fail-fast机制：      CAP              -->     CP   高一致性      ArrayList, HashSet,HashMap
 * 基于迭代器一边遍历集合，一边修改集合长度，会抛出并发修改异常：ConcurrentModificationException
 *
 * 集合的un-safe机制：        CAP              -->     AP   高可用性   CopyOnWriteArrayList, CopyOnWriteArraySet,ConcurrentHashMap
 *
 *  cursor: 迭代器迭代次数  -- 已经迭代了几次，初始值为0
 *  expectedModCount: 集合的预期修改次数   4
 *  modCount: 集合的实际修改次数           5
 */

public class Demo {
    public static void main(String[] args) {
//        ArrayList<String> list = new ArrayList<>();
        CopyOnWriteArrayList<String> list = new CopyOnWriteArrayList<>();

        list.add("hello");
        list.add("world");
        list.add("java");
        list.add("JavaWeb");

        Iterator<String> it = list.iterator();
        while (it.hasNext()){
            String s = it.next();           //查询的是老数组
            System.out.println("s = " + s);

            if ("world".equals(s)){
                list.add("JavaSE");     // 添加到新数组
            }
        }
    }
}
