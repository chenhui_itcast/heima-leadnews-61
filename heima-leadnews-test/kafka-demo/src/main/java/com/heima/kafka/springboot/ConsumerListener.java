package com.heima.kafka.springboot;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * @author jack
 * @data 2023 12:13
 */
@Component
public class ConsumerListener {

    @KafkaListener(topics = "itcast")
    public void listenMsg(String msg){
        System.out.println("监听到msg ： " + msg);
    }
}
