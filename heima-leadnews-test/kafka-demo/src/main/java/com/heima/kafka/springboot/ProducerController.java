package com.heima.kafka.springboot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jack
 * @data 2023 12:10
 */

@RestController
public class ProducerController {

    @Autowired
    private KafkaTemplate<String,String> kafkaTemplate;


    @GetMapping("/send")
    public String sendMsg(){
        kafkaTemplate.send("itcast",0,"key2","hello,kafka");
        return "success";
    }
}
