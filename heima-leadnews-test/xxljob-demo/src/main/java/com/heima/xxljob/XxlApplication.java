package com.heima.xxljob;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author jack
 * @data 2023 10:38
 */
@SpringBootApplication
public class XxlApplication {
    public static void main(String[] args) {
        SpringApplication.run(XxlApplication.class,args);
    }
}
