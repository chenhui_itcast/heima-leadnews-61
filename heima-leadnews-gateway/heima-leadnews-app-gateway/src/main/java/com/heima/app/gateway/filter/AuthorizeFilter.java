package com.heima.app.gateway.filter;

import com.heima.app.gateway.util.AppJwtUtil;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * 基于全局过滤器实现统一登录验证
 */
@Slf4j
@Component
public class AuthorizeFilter implements GlobalFilter {

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        log.info("接收到请求了:{}",exchange.getRequest().getPath());

        //1. 先拿到请求request,响应response对象
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();

        //2. 通过request拿到本次请求路径
        String path = request.getPath().toString();

        //3. 如果是登录相关的请求，直接放行
        if (path.contains("login")){
            //放行
            return chain.filter(exchange);
        }

        //4. 不是登录相关，就需要校验：token
        //4.1 从请求头中获取token
        String token = request.getHeaders().getFirst("Token");

        /**
         * 4.2 无效： 响应一个状态码：401
         *      有效：
         *          a. 不能为空
         *          b. 不能是伪造的，不能是过期的!
         */
        //不能为空
        if (StringUtils.isEmpty(token)){
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return response.setComplete();
        }
        //不能是伪造的
        Claims claimsBody = AppJwtUtil.getClaimsBody(token);
        if (claimsBody == null){
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return response.setComplete();
        }
        //不能是过期的!
        int res = AppJwtUtil.verifyToken(claimsBody);
        if (res == 1 || res == 2){
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return response.setComplete();
        }


        //没有过期：取出userId,作为请求头，传给下游微服务
        Object userId = claimsBody.get("id");
        //在header中添加新的信息
        ServerHttpRequest serverHttpRequest = request.mutate().headers(httpHeaders -> {
            httpHeaders.add("userId", userId + "");
        }).build();

        //重置header
        exchange.mutate().request(serverHttpRequest).build();

        //4.3 token有效： 放行
        //放行
        return chain.filter(exchange);
    }
}
